import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarContratosPage } from './listar-contratos.page';

describe('ListarContratosPage', () => {
  let component: ListarContratosPage;
  let fixture: ComponentFixture<ListarContratosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarContratosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarContratosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
