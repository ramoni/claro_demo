import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ListarContratosPage } from './listar-contratos.page';

const routes: Routes = [
  {
    path: '',
    component: ListarContratosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),

  ],
  declarations: [ListarContratosPage]
})
export class ListarContratosPageModule {
  
}
