import { Component, OnInit } from '@angular/core';
import { PdfServicesPage } from '../pdf-services/pdf-services.page';
import { LoadingController } from '@ionic/angular';
import { HttpServicesPage } from '../http-services/http-services.page';

declare var cordova: any;

@Component({
  selector: 'app-listar-contratos',
  templateUrl: './listar-contratos.page.html',
  styleUrls: ['./listar-contratos.page.scss'],
})

export class ListarContratosPage implements OnInit {
  constructor(public httpService: HttpServicesPage, public pdf: PdfServicesPage, public loadingCtrl: LoadingController) {
      this.listarContratos();
  }

  contratos: any;
  buscar:string;
  contratosFilter: any;
  NRO_CI:string;
  
  //apiUrlPDF =  "https://test.bypersoft.com/yii/web/index.php?r=status%2Freport-base&ci=";
  apiUrlPDF = "https://claro.jfwebportal.com/yii/web/index.php?r=status%2Freport-base&ci=";
  
  ngOnInit() {
  
  }
  setFilterContratos() {

    this.contratosFilter = this.httpService.filtrarBusqueda(this.buscar,this.contratos);
    
  }

  listarContratos() {
    let respuesta = this.httpService.getLista();
    console.log(respuesta);
    respuesta.then((res) => {
      //console.log("no definido" + res);
      //console.log("a ver" + JSON.parse(JSON.stringify(res)));
      this.contratosFilter = JSON.parse(res);
      this.contratos = JSON.parse(res);
    }).catch(res => {
      console.log(res);
    });

  }
  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      //duration: x,
      //message: 'Espera...',
      translucent: true,
      cssClass: 'confirm-color'
    });
    return await loading.present();
  }
  obtenerPDF(ci) {
    this.presentLoading();
    let params = {};
    let headers = {};
    cordova.plugin.http.get(this.apiUrlPDF + ci,
      params, headers, (response) => {
        let pdfBase64 = JSON.parse(response.data);
        console.log("datos del get para pdf" + response.data);
        this.pdf.verPDF(pdfBase64.data, "x.pdf");
        this.loadingCtrl.dismiss();
      }, function (response) {
        console.error("entro en error pdf" + response.error);
      });
  }

  doRefresh(event) {
    //console.log('Begin async operation');
    this.buscar = '';
    this.listarContratos();
    setTimeout(() => {
      //console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
}
