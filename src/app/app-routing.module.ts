import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'formulario', loadChildren: './formulario/formulario.module#FormularioPageModule' },
  { path: 'firma', loadChildren: './firma/firma.module#FirmaPageModule' },
  { path: 'listar-contratos', loadChildren: './listar-contratos/listar-contratos.module#ListarContratosPageModule' },
  { path: 'httpServices', loadChildren: './http-services/http-services.module#HttpServicesPageModule' },
  { path: 'pdf-services', loadChildren: './pdf-services/pdf-services.module#PdfServicesPageModule' },
  { path: 'no-used', loadChildren: './no-used/no-used.module#NoUsedPageModule' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
