import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfServicesPage } from './pdf-services.page';

describe('PdfServicesPage', () => {
  let component: PdfServicesPage;
  let fixture: ComponentFixture<PdfServicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfServicesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfServicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
