import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Router } from '@angular/router';
@Component({
  selector: 'app-pdf-services',
  templateUrl: './pdf-services.page.html',
  styleUrls: ['./pdf-services.page.scss'],
})
export class PdfServicesPage implements OnInit {

  constructor( private plt: Platform, public file: File, public fileOpener: FileOpener) { }

  ngOnInit() {
  }

  verPDF(pdfBase64: string, nombreArchivo: string) {
    //console.log("entro para verPDF " + pdfBase64);
    if (this.plt.is('cordova')) {
      this.file.writeFile(this.file.dataDirectory, nombreArchivo, this.Base64_A_Blob(pdfBase64, 'data:application/pdf;base64'), { replace: true })
        .then(() => {
          this.fileOpener.open(this.file.dataDirectory + nombreArchivo, 'application/pdf')
            .catch(() => {
              console.log('Error opening pdf file');
              
            });
           
        })
        .catch(() => {
          console.error('Error writing pdf file');

        });
    } else {
      window.open(pdfBase64);
    }
  }
  /*
  Base64_A_Blob1(b64Data, contentType): Blob {
    contentType = contentType || '';
    const sliceSize = 512;
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');
    const byteCharacters = window.atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, { type: contentType });
  }*/
  Base64_A_Blob(b64Data, contentType): Blob {
    contentType = contentType || '';
    var sliceSize = 1024;
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');
    var byteCharacters = atob(b64Data);
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
        var begin = sliceIndex * sliceSize;
        var end = Math.min(begin + sliceSize, bytesLength);
        var bytes = new Array(end - begin);
        for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
        }
        byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
  }
}
