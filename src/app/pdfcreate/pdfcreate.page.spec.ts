import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfcreatePage } from './pdfcreate.page';

describe('PdfcreatePage', () => {
  let component: PdfcreatePage;
  let fixture: ComponentFixture<PdfcreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfcreatePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfcreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
