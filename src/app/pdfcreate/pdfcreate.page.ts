import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { ActivatedRoute } from '@angular/router';
import { DocumentViewer, DocumentViewerOptions} from '@ionic-native/document-viewer/ngx';
@Component({
  selector: 'app-pdfcreate',
  templateUrl: './pdfcreate.page.html',
  styleUrls: ['./pdfcreate.page.scss'],
})
export class PdfcreatePage implements OnInit {
  constructor(private document: DocumentViewer, private plt: Platform, public file: File, public fileOpener: FileOpener, private route: ActivatedRoute) { }
  datos: any;

  pdfObj = null;
  ngOnInit() {
    this.datos = this.route.snapshot.paramMap;
    //this.createPdf();
   // console.log("paso crear pdf");
   // this.downloadPdf();
    //this.documentView();
  }

  documentView() {
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    }
    this.document.viewDocument(this.pdfObj, 'aplication/pdf',options);
    //this.document.viewDocument(this.file.dataDirectory+'Contrato.pdf', 'application/pdf', options);
  }
  createPdf() {
  
    var docDefinition = {
      content: [
        {text: 'Claro', style: 'header' },
        {text: 'Contrato '+this.datos.params.contrato+'\n\n', style: 'subheader' },
        
       // { text: new Date().toTimeString(), alignment: 'right' },
  
        { text: 'Nombre y Apellido: ' + this.datos.params.nombres +' ' + this.datos.params.apellidos + '\n\n', style: 'story' },
       
        { text: 'Ci: ' + this.datos.params.ci+'\n\n', style: 'story' },

        { text: 'Direccion: ' +this.datos.params.direccion +'\n\n', style: 'story' },
     
        { text: 'Email: '+this.datos.params.email+'\n\n', style: 'story' },

        {text:'Firma \n', style:'story'},
        {image: this.datos.params.signature, width: 100,alignment:'center'},
       {text: '\nAdjunto de Cedula\n\n',style:'story',},
      {image: this.datos.params.cifrontal, width: 150, height:150},
      {image: this.datos.params.citrasera, width: 150, height:150},
      ],
      styles: {
        header: {
          fontSize: 30,
          bold: true,
          fontColor: '#f324f1',
          alignment: 'center',
        },
        subheader: {
          alignment: 'center',
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'left',
          width: '50%',
        }
      }
    }
    this.pdfObj = pdfMake.createPdf(docDefinition);
    
  }

  downloadPdf() {
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });

        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'Contrato.pdf', blob, { replace: true })//.then(fileEntry => {
          // Open the PDf with the correct OS tools
         // this.fileOpener.open(this.file.dataDirectory + 'contrato.pdf', 'application/pdf');
        //})
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }

}
