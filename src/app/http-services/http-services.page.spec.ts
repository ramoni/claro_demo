import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpServicesPage } from './http-services.page';

describe('HttpServicesPage', () => {
  let component: HttpServicesPage;
  let fixture: ComponentFixture<HttpServicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HttpServicesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpServicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
