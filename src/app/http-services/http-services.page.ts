import { Component, OnInit, Injectable } from '@angular/core';
import { RequestOptions, Headers, Http } from '@angular/http';//para metodo post
import { catchError, map } from 'rxjs/operators';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'; //para metodo get
import { throwError, Observable } from 'rxjs';
import { PdfServicesPage } from '../pdf-services/pdf-services.page';
import { Router } from '@angular/router';
declare var cordova: any;
@Component({
  selector: 'app-http-services',
  templateUrl: './http-services.page.html',
  styleUrls: ['./http-services.page.scss'],
})

@Injectable()
export class HttpServicesPage implements OnInit {
  
  constructor(private route: Router, public pdf: PdfServicesPage, private http: HTTP) { 
    //this.getLista();
  }
  ngOnInit() {
  }
  //apiUrlPost = "https://test.bypersoft.com/yii/web/index.php?r=status%2Fsave-status";
  //apiUrlPDF = "https://test.bypersoft.com/yii/web/index.php?r=status%2Freport-base&ci=";
  //apiUrlLISTA = "https://test.bypersoft.com/yii/web/index.php?r=status%2Fget-status-json";
  apiUrlLISTA = "https://claro.jfwebportal.com/yii/web/index.php?r=status%2Fget-status-json";
  apiUrlPost = "https://claro.jfwebportal.com/yii/web/index.php?r=status%2Fsave-status";
  apiUrlPDF = "https://claro.jfwebportal.com/yii/web/index.php?r=status%2Freport-base&ci=";

  
  filtrarBusqueda(search, contratos) {
    if(!contratos) return [];
    if(!search) return contratos;
    return contratos.filter((contr) => {
      console.log("search "+search);
      return contr.NRO_CI.toLowerCase().indexOf(search.toLowerCase()) !==-1  ||
      contr.NOMBRE.toLowerCase().indexOf(search.toLowerCase()) !== -1  ||
      contr.APELLIDOS.toLowerCase().indexOf(search.toLowerCase()) !==-1;
    });
  }
  getLista() {
    return this.http.get(this.apiUrlLISTA, {}, {}).then(response => {
      console.log("datos del get para contratos " + response.data);
      //this.contratos=JSON.parse(response.data)
      //this.contratos2=JSON.parse(response.data)
      return Promise.resolve(response.data);
    }).catch(response => {
      console.error("entro en error " + response.error);
      return Promise.reject(response.error)
    });
  }
  public postHTTP(datos: any) {
    let dato = {
      NOMBRE: "nose",
      NRO_CI: "79768",
      APELLIDOS: "xxxx"
    }
    this.http.clearCookies();
    this.http.setDataSerializer('json');
    this.http.post(this.apiUrlPost, datos, { "Accept": 'application/json', "Content-Type": "application/json" })
      .then(resp => {
        console.log(resp.status);
        console.log(resp.data);
        this.obtenerPDF(datos.NRO_CI);
      }).catch(error => {
        console.log(error.error);
      });
  }

  obtenerPDF(ci) {
    cordova.plugin.http.get(this.apiUrlPDF + ci, {}, {}, (response) => {
      let pdfBase64 = JSON.parse(response.data);
      console.log("datos del get para pdf" + response.data);
      this.pdf.verPDF(pdfBase64.data, ci + ".pdf");
      this.route.navigate(['home']);
    }, function (response) {
      console.error("entro en error pdf" + response.error);
    });
  }

}