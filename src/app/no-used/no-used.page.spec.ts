import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoUsedPage } from './no-used.page';

describe('NoUsedPage', () => {
  let component: NoUsedPage;
  let fixture: ComponentFixture<NoUsedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoUsedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoUsedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
