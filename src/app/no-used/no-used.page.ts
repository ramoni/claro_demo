import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { RequestOptions, Http } from '@angular/http';
import { HttpHeaders, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
declare var cordova:any;
@Component({
  selector: 'app-no-used',
  templateUrl: './no-used.page.html',
  styleUrls: ['./no-used.page.scss'],
})
export class NoUsedPage implements OnInit {
  datos: any;
  pdfObj = null;
  apiUrlPost = "https://test.bypersoft.com/datos/post.php";
  constructor( private plt: Platform,private document: DocumentViewer, 
    public file: File, public fileOpener: FileOpener,
    private httpGet: HttpClient, private httpPost: Http) { }

  ngOnInit() {
  }

  documentView() {
    const options: DocumentViewerOptions = {
      title: 'Contrato'
    }
    this.document.viewDocument('assets/docs/Contrato.pdf', 'application/pdf', options);
  }
  createPdf() {

    var docDefinition = {
      content: [
        { text: 'Claro', style: 'header' },
        { text: 'Contrato ' + this.datos.contrato + '\n\n', style: 'subheader' },
        // { text: new Date().toTimeString(), alignment: 'right' },
        {
          table: {
            style: 'normal',
            body: [
              [{ text: 'Nombre y Apellido: ', style: 'story' }, { text: this.datos.nombres + ' ' + this.datos.apellidos }],
              [{ text: 'Ci: ', style: 'story' }, { text: this.datos.ci }],
              [{ text: 'Direccion: ', style: 'story' }, { text: this.datos.direccion }],
              [{ text: 'Email: ', style: 'story' }, { text: this.datos.email }],
            ]
          },

        },
        { text: '\n\nFirma \n', style: 'story' },
        { image: this.datos.signature, width: 100, alignment: 'center' },
        { text: '\nAdjunto de Cedula\n\n', style: 'story', },
        {
          columns: [
            {
              width: '*',
              stack: [{
                image: this.datos.cifrontal,
                width: 250,
                height: 150
              }]
            },
            {
              width: '*',

              stack: [{
                image: this.datos.citrasera,
                width: 250,
                height: 150

              }]
            }
          ]
        },

      ],
      styles: {
        header: {
          fontSize: 50,
          bold: true,
          fontColor: '#f324f1',
          alignment: 'center',
        },
        subheader: {
          alignment: 'center',
          fontSize: 30,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          bold: true,
          italic: true,
          alignment: 'left',
          width: '50%',
          fontSize: 20,
        }
      }
    }
    this.pdfObj = pdfMake.createPdf(docDefinition);

  }
  downloadPdf() {
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
        console.log(this.file.applicationDirectory);
        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'Contrato.pdf', blob, { replace: true }).then(fileEntry => {
          console.log("escribio el archivo");
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'Contrato.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE'
    })
  };
  
  getContratos(Url) {
    return this.httpGet.get(Url, this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }
  enviarPOST(datos: any) {
    const options = {
      method: 'post',
      data: {NOMBRE:'Ester', CI:'6854332'},
      headers: { Accept: "application/json" }
    };
    cordova.plugin.http.sendRequest(this.apiUrlPost, options, function (response) {
      // prints 200
      console.log(response.data);
      console.log(response.status);
    }, function (response) {
      // prints 403
      console.log(response.status);  
      //prints Permission denied
      console.log(response.error);
    });

  }
  enviarPOSTHttp(datos: any) {
    console.log(datos);
    var header = new Headers();
    header.append("Accept", 'application/json');
    header.append('Content-Type', 'application/json');
    //const requestOptions = new RequestOptions({ Headers: header });

    this.httpPost.post(this.apiUrlPost, datos, {}).subscribe(data => {
      console.log(data['_body']);
    }, error => {
      console.log(error);
    });

  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

}
