import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { HttpServicesPage } from '../http-services/http-services.page';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.page.html',
  styleUrls: ['./formulario.page.scss'],
})

export class FormularioPage implements OnInit {
  apiUrlPost = "https://claro.jfwebportal.com/yii/web/index.php?r=status%2Fsave-status";
  datos = {

    NOMBRE: ' ',
    APELLIDOS: ' ',
    NRO_CI: ' ',
    CI_FIRMA: ' ',
    DIRECCION: ' ',
    EMAIL: ' ',
    CI_FRENTE: ' ',
    CI_ATRAS: ' ',
    TIPO_CONTRATO: ' ',

  }

  ngOnInit() {
    this.datos.TIPO_CONTRATO = this.ruta.snapshot.paramMap.get('TIPO_CONTRATO');
    this.datos.CI_FIRMA = this.ruta.snapshot.paramMap.get('CI_FIRMA');
    this.datos.NOMBRE = this.ruta.snapshot.paramMap.get('NOMBRE');
    this.datos.APELLIDOS = this.ruta.snapshot.paramMap.get('APELLIDOS');
    this.datos.EMAIL = this.ruta.snapshot.paramMap.get('EMAIL');
    this.datos.DIRECCION = this.ruta.snapshot.paramMap.get('DIRECCION');
    this.datos.NRO_CI = this.ruta.snapshot.paramMap.get('NRO_CI');
    this.datos.CI_FRENTE = this.ruta.snapshot.paramMap.get('CI_FRENTE');
    this.datos.CI_ATRAS = this.ruta.snapshot.paramMap.get('CI_ATRAS');

    //console.log('formulario init', this.datos);
  }
  constructor(private http:HTTP, public loadingCtrl: LoadingController, public alertCtrl: AlertController, private httpServices: HttpServicesPage, private camara: Camera, private route: Router, public ruta: ActivatedRoute) {
  }
  /*enviarDatos() {
    this.presentarConfirmation();
  }*/
  async presentarConfirmation() {
    const alert = await this.alertCtrl.create({
      header: 'Crear Contrato',
      message: '¿Deseas Crear un nuevo Contrato?',

      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'confirm-color',
          handler: () => {
            // Ha respondido que no así que no hacemos nada
          }
        },
        {
          text: 'Si',
          cssClass: 'confirm-color',
          handler: () => {
            this.postHTTP();
            this.presentLoading();
            
          }
        }
      ]
    });
    await alert.present();
  }
  public postHTTP() {
    let dato={
      NOMBRE:"nose",
      NRO_CI: "79768",
      APELLIDOS: "xxxx"
    }
    this.http.clearCookies();
    this.http.setDataSerializer('json');
    this.http.post(this.apiUrlPost, this.datos, {"Accept": 'application/json',"Content-Type":"application/json"})
          .then(resp => {
            console.log(resp.status);
            console.log(resp.data);
            this.httpServices.obtenerPDF(this.datos.NRO_CI);
            this.datos = {

              NOMBRE: ' ',
              APELLIDOS: ' ',
              NRO_CI: ' ',
              CI_FIRMA: ' ',
              DIRECCION: ' ',
              EMAIL: ' ',
              CI_FRENTE: ' ',
              CI_ATRAS: ' ',
              TIPO_CONTRATO: ' ',
          
            }
            this.loadingCtrl.dismiss();
          }).catch(error=>{
            console.log(error.error);
          });
}

  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      //duration: 24000,
     // message: 'Espera...',
      translucent: true,
      cssClass: 'confirm-color'
    });
    return await loading.present();
  }

  getPictureFrontal() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camara.DestinationType.DATA_URL,
      encodingType: this.camara.EncodingType.JPEG,
      mediaType: this.camara.MediaType.PICTURE,
      targetWidth: 500,
      targetHeight: 500,
    }
    this.camara.getPicture(options).then(imageData => {
      this.datos.CI_FRENTE = 'data:image/jpeg;base64,' + imageData;
    }).catch(error => {
      console.error(error);
    });
  }
  getPictureDorsal() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camara.DestinationType.DATA_URL,
      encodingType: this.camara.EncodingType.JPEG,
      mediaType: this.camara.MediaType.PICTURE,
      targetWidth: 500,
      targetHeight: 500,
    }
    this.camara.getPicture(options).then(imageData => {
      this.datos.CI_ATRAS = 'data:image/jpeg;base64,' + imageData;
    }).catch(error => {
      console.error(error);
    });
  }
  openSignatureModel() {
    this.route.navigate(["firma", this.datos]);
  }
}