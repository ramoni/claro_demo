import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(contratosFilter: any[], buscar: string): any[] {
    if(!contratosFilter) return [];
    if(!buscar) return contratosFilter;
    buscar = buscar.toLowerCase();
    return contratosFilter.filter( it => {
      return it.NRO_CI.toLowerCase().includes(buscar); 
    });
  }

}
