import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, RouteReuseStrategy, Routes } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Camera } from '@ionic-native/camera/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { PdfServicesPage } from './pdf-services/pdf-services.page';
import { HttpServicesPage } from './http-services/http-services.page';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { SearchPipe } from './search.pipe'

@NgModule({
  declarations: [AppComponent, SearchPipe],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    FileOpener,
    File,
    DocumentViewer,
    HttpServicesPage,
    PdfServicesPage,
    Toast,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  exports:[SearchPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
