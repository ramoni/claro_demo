import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
//import { IonicStorageModule } from '@ionic/storage';
import { IonicModule } from '@ionic/angular';
import { Toast } from '@ionic-native/toast/ngx';
import { FirmaPage } from './firma.page';
import { SignaturePadModule } from 'angular2-signaturepad';
//import { Storage } from '@ionic/storage';

const routes: Routes = [  
  {
    path: '',
    component: FirmaPage
  },
  
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SignaturePadModule, 
  ],
  //providers:[Toast],
  declarations: [FirmaPage]
})
export class FirmaPageModule {}
