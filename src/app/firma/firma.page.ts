import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Router, ActivatedRoute } from '@angular/router';
import { Toast } from '@ionic-native/toast/ngx';

@Component({
  selector: 'app-firma',
  templateUrl: './firma.page.html',
  styleUrls: ['./firma.page.scss'],
})
export class FirmaPage implements OnInit {
  CI_FIRMA = '';
  isDrawing = false;

  datos = {
    NOMBRE: ' ',
    APELLIDOS: ' ',
    NRO_CI: ' ',
    CI_FIRMA: ' ',
    DIRECCION: ' ',
    EMAIL: ' ',
    CI_FRENTE: ' ',
    CI_ATRAS: ' ',
    TIPO_CONTRATO: ' ',
  }

  constructor(public route: Router, public ruta: ActivatedRoute, private toastCtrl: Toast) { }

  ngOnInit() {
    this.datos.TIPO_CONTRATO = this.ruta.snapshot.paramMap.get('TIPO_CONTRATO');
    this.datos.CI_FIRMA = this.ruta.snapshot.paramMap.get('CI_FIRMA');
    this.datos.NOMBRE = this.ruta.snapshot.paramMap.get('NOMBRE');
    this.datos.APELLIDOS = this.ruta.snapshot.paramMap.get('APELLIDOS');
    this.datos.EMAIL = this.ruta.snapshot.paramMap.get('EMAIL');
    this.datos.DIRECCION = this.ruta.snapshot.paramMap.get('DIRECCION');
    this.datos.NRO_CI = this.ruta.snapshot.paramMap.get('NRO_CI');
    this.datos.CI_FRENTE = this.ruta.snapshot.paramMap.get('CI_FRENTE');
    this.datos.CI_ATRAS = this.ruta.snapshot.paramMap.get('CI_ATRAS');

    //console.log('init firma',this.datos);
  }

  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  @Input()
  public signaturePadOptions: Object = {
    'minWidth': 0.2,
    //'canvasWidth': 230,
    'canvasHeight': 400,
    'backgroundColor': '#ffffff',
    'penColor': '#000000'
  };

  drawComplete() {
    this.isDrawing = false;
  }

  drawStart() {
    this.isDrawing = true;
  }
  savePad() {
    this.CI_FIRMA = this.signaturePad.toDataURL();
    this.signaturePad.clear();
    let toast = this.toastCtrl.show('Firma Guardada', '1000', 'center').subscribe(
      toast => {
        console.log(toast);
      }
    );
    console.log('en savepad', this.datos);
    this.datos.CI_FIRMA = this.CI_FIRMA;
    this.route.navigate(['formulario', this.datos]);
  }

  clearPad() {
    this.signaturePad.clear();
  }
}