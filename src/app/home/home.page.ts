import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { File } from '@ionic-native/file/ngx';
import { DocumentViewerOptions, DocumentViewer} from '@ionic-native/document-viewer/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(public ruta: Router,private document:DocumentViewer) {}

  irAFormulario() {
    this.ruta.navigate(["formulario"]);
  }

}
